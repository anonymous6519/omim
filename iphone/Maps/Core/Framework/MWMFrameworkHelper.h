@interface MWMFrameworkHelper : NSObject

+ (void)zoomToCurrentPosition;

+ (void)setVisibleViewport:(CGRect)rect;

@end
